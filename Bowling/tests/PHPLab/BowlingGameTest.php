<?php
namespace PHPLab;

class BowlingGameTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var BowlingGame
     */
    protected $game;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->game = new \PHPLab\BowlingGame;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers PHPLab\BowlingGame::roll
     */
    public function testRoll() {
        
    }
    
    public function testZeroScore() {
        $this->assertTrue($this->rollNum(20, 0), $this->error);
        $this->assertEquals(0, $this->game->score());
    }
    
    public function testOnePointPerRoll() {
        $this->assertTrue($this->rollNum(20, 1), $this->error);
        $this->assertEquals(20, $this->game->score());
    }
    
    public function testSpareScoring() {
        try {
            // 1st roll
            $this->game->roll(0);
            $this->game->roll(0);
            
            // 2nd roll
            $this->game->roll(3);
            $this->game->roll(7);
            
            // 3rd roll
            $this->game->roll(3);
            $this->game->roll(0);
            
            // Other rolls
            $this->assertTrue($this->rollNum(15, 0), $this->error);
        } catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
        
        $this->assertEquals(16, $this->game->score());
    }
    
    public function testStrikeScoring() {
        try {
            // 1st roll
            $this->game->roll(10);
            
            // 2nd roll
            $this->game->roll(3);
            $this->game->roll(4);
            
            // Other rolls
            $this->assertTrue($this->rollNum(17, 0), $this->error);
        } catch (\Exception $e) {
            $this->fail($this->getMessage());
        }
        
        $this->assertEquals(24, $this->game->score());
    }
    
    public function testAllStrikes() {
        for ($roll = 0; $roll <= 11; $roll++) {
            try {
                $this->game->roll(10);
            } catch (\Exception $e) {
                $this->fail($e->getMessage());
            }
        }
        
        $this->assertEquals(300, $this->game->score());
    }
    
    /**
     * Attempts to roll $score as many times as $num
     */
    private function rollNum($num, $score) {
        $this->error = null;
        
        try {
            for ($roll = 1; $roll <= $num; $roll++) {
                $this->game->roll($score);
            }
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }
        
        return true;
    }
}
