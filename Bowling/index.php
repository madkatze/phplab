<?php

include 'vendor/autoload.php';

$game = new \PHPLab\BowlingGame();

$scores = array(
    array(10),
    array(10),
    array(10),
    
    array(10),
    array(10),
    array(10),
    
    array(10),
    array(10),
    array(10),
    
    array(10),
    
    array(10),
    array(10)
);

foreach ($scores as $score) {
    $game->roll($score[0]);
    if (isset($score[1])) {
        $game->roll($score[1]);
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo $game->score();
        ?>
    </body>
</html>