<?php

namespace PHPLab;

/**
 * Bowling game score calculator
 *
 * @author Todor
 */
class BowlingGame {
	private $rolls		= array();
    private $frames     = array();
    
    private $rollCount  = 0;
    
    public function roll($rollScore) {
        if ($this->rollCount > 21) {
            throw new \Exception("Roll count limit exceeded!");
            return;
        }
        
        if ($rollScore < 0 || $rollScore > 10) {
            throw new \Exception("Roll score must be between 0 and 10.");
            return;
        }
        
        $this->rolls[] = $rollScore;
        
        $this->rollCount++;
    }
    
    public function score() { 
        $totalScore = 0;
        
        if (!$this->buildFrames()) {
            return 0;
        }
        
        foreach ($this->frames as $framePos => $frameData) {
            $firstRoll  = isset($frameData[0]) ? $frameData[0] : 0;
            $secondRoll = isset($frameData[1]) ? $frameData[1] : 0;
            
            if ($firstRoll == 10) {
                $totalScore += $this->strikeBonus($framePos);
            } elseif ($firstRoll + $secondRoll == 10) {
                $totalScore += $this->spareBonus($framePos);
            } else {
                $totalScore += ($firstRoll + $secondRoll);
            }
        }
        
        return $totalScore;
    }
    
    private function buildFrames() {        
        // Build data about played frames
        for ($rollPos = 0, $framePos = 0; $rollPos < $this->rollCount; $rollPos++) {
            if (!isset($this->rolls[$rollPos])) {
                // Skip non-existing rolls
                continue;
            }
            
            if ($this->rolls[$rollPos] == 10) {
                $this->frames[$framePos] = array(10, 0);
                
                $framePos++;
            } else {
                if (isset($this->frames[$framePos][1])) {
                    $framePos++;
                }
                
                if (isset($this->frames[$framePos][0])) {
                    if ($this->frames[$framePos][0] + $this->rolls[$rollPos] > 10) {
                        throw new \Exception("Frame score combined cannot exceed 10 points!");
                        return false;
                    }
                }
                
                $this->frames[$framePos][] = $this->rolls[$rollPos];
            }
        }
        
         return true;
    }
    
    private function strikeBonus($framePos) {
        $bonusScore = 10;
        
        if ($framePos == 9) {
            // 10nth frame
            $bonusScore += isset($this->rolls[19]) ? $this->rolls[19] : 0;
            $bonusScore += isset($this->rolls[20]) ? $this->rolls[20] : 0;
        } else {
            if (isset($this->frames[$framePos + 1])) {
                // Fix bug when all is strike
                if ($framePos + 1 != 11) {
                    if ($this->frames[$framePos + 1][0] == 10) {
                        $bonusScore += 10;

                        if (isset($this->frames[$framePos + 2][0])) {
                            $bonusScore += $this->frames[$framePos + 2][0];
                        }
                    } else {
                        $bonusScore += $this->frames[$framePos + 1][0];
                        $bonusScore += (isset($this->frames[$framePos + 1][1]) ? $this->frames[$framePos + 1][1] : 0);
                    }
                }
            }
        }
        
        return $bonusScore;
	}
    
    private function spareBonus($framePos) {
		$bonusScore = 10;
        
        if ($framePos == 9) {
            // 10nth frame
            $bonusScore += (isset($this->rolls[20]) ? $this->rolls[20] : 0);
        } else {
            if (isset($this->frames[$framePos + 1])) {
                $bonusScore += $this->frames[$framePos + 1][0];
            }
        }
        
        return $bonusScore;
	}
}

?>